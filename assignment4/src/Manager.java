package com.rest;

public class Manager {
	
	private String Managername;
	private String Managerid;
	private String phoneno;
	public String getManagername() {
		return Managername;
	}
	public void setManagername(String managername) {
		Managername = managername;
	}
	public String getManagerid() {
		return Managerid;
	}
	public void setManagerid(String managerid) {
		Managerid = managerid;
	}
	public String getPhoneno() {
		return phoneno;
	}
	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}
}